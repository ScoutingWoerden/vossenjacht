<?php

class Controller{
    protected $f3;
    protected $db;

// Function thats needs to be done before routing the whole page.
    function beforeroute(){
        if ($this->f3->get('SESSION.email') == null) {
            $this->f3->reroute('/login');
            exit;
        }
    }

// Function thats need to be done after routing the whole page.
    function afterroute(){

    }

    function __construct(){
        $f3=Base::instance();
        $db=new DB\SQL(
            $f3->get('dbconn'),
            $f3->get('dbuser'),
            $f3->get('dbpass')
            //array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
        );
        $this->f3=$f3;
        $this->db=$db;
    }
}