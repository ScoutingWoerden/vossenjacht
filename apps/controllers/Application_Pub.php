<?php

class Application_Pub extends Controller{

// Function thats needs to be done before routing the whole page.
    function beforeroute(){
    }

// Function
    function remainingTime(){
        $endOfSemester = mktime($this->f3->get('app.when_hour'),$this->f3->get('app.when_minute'),0,$this->f3->get('app.when_month'),$this->f3->get('app.when_day'),$this->f3->get('app.when_year'));
        $now = time();
        $secondsRemaining = $endOfSemester - $now;

        define('SECONDS_PER_MINUTE', 60);
        define('SECONDS_PER_HOUR', 3600);
        define('SECONDS_PER_DAY', 86400);

        $daysRemaining = floor($secondsRemaining / SECONDS_PER_DAY); //days until end
        $secondsRemaining -= ($daysRemaining * SECONDS_PER_DAY); //update variable

        $hoursRemaining = floor($secondsRemaining / SECONDS_PER_HOUR); //hours until end
        $secondsRemaining -= ($hoursRemaining * SECONDS_PER_HOUR); //update variable

        $minutesRemaining = floor($secondsRemaining / SECONDS_PER_MINUTE); //minutes until end
        $secondsRemaining -= ($minutesRemaining * SECONDS_PER_MINUTE); //update variable

        return("Nog $daysRemaining dagen $hoursRemaining uren $minutesRemaining minuten $secondsRemaining seconds");
    }

// Function to show user some information.
    function Home(){
        //Base
        $functions = new Functions;
        $this->f3->set('title', 'Home');
        $this->f3->set('active', 'home');
        $this->f3->set('view', 'application_pub/home.htm');

        //App
        $this->f3->set('remaining', $this->remainingTime());

        //Render
        $template = new Template;
        echo $template->render('template_apppub.htm');
    }
// Function to show user some information.
    function How(){
        //Base
        $this->f3->set('title', 'Hoe werkt het?');
        $this->f3->set('active', 'how');
        $this->f3->set('view', 'application_pub/how.htm');

        //App
        $this->f3->set('remaining', $this->remainingTime());

        //Render
        $template = new Template;
        echo $template->render('template_apppub.htm');
    }

// Function for emailing.
    function Ranklist(){
        //Base
        $this->f3->set('title', 'Ranklijst');
        $this->f3->set('active', 'ranklist');
        $this->f3->set('view', 'application_pub/ranklist.htm');

        //App
        $this->f3->set('remaining', $this->remainingTime());

        $ranklist_easy = $this->db->exec("Select u.*, RANK() OVER (ORDER BY correct desc, aangemaakt DESC) AS rank, COUNT(CASE WHEN a.answer = q.answer THEN 1 END) AS correct, COUNT(CASE WHEN a.answer != q.answer THEN 1 END) AS incorrect, (SELECT COUNT(*) FROM questions) as total from users as u LEFT JOIN user_answers as a on u.id=a.uid LEFT JOIN questions as q on a.qid=q.id where u.lvl=1 group by u.id order by rank asc;");
        $this->f3->set('ranklist_easy', $ranklist_easy);

        $ranklist_normal = $this->db->exec("Select u.*, RANK() OVER (ORDER BY correct desc, aangemaakt DESC) AS rank, COUNT(CASE WHEN a.answer = q.answer THEN 1 END) AS correct, COUNT(CASE WHEN a.answer != q.answer THEN 1 END) AS incorrect, (SELECT COUNT(*) FROM questions) as total from users as u LEFT JOIN user_answers as a on u.id=a.uid LEFT JOIN questions as q on a.qid=q.id where u.lvl=2 group by u.id order by rank asc;");
        $this->f3->set('ranklist_normal', $ranklist_normal);

        $ranklist_hard = $this->db->exec("Select u.*, RANK() OVER (ORDER BY correct desc, aangemaakt DESC) AS rank, COUNT(CASE WHEN a.answer = q.answer THEN 1 END) AS correct, COUNT(CASE WHEN a.answer != q.answer THEN 1 END) AS incorrect, (SELECT COUNT(*) FROM questions) as total from users as u LEFT JOIN user_answers as a on u.id=a.uid LEFT JOIN questions as q on a.qid=q.id where u.lvl=3 group by u.id order by rank asc;");
        $this->f3->set('ranklist_hard', $ranklist_hard);

        //Render
        $template = new Template;
        echo $template->render('template_apppub.htm');
    }

// Function for emailing.
    function Mail(){
        if($this->f3->exists('POST.mail')){
            $receiver = new SMTP ( $this->f3->get('smtp.host'), $this->f3->get('smtp.port'), 'SSL', $this->f3->get('smtp.user'), $this->f3->get('smtp.pw') );
            $sender = new SMTP ( $this->f3->get('smtp.host'), $this->f3->get('smtp.port'), 'SSL', $this->f3->get('smtp.user'), $this->f3->get('smtp.pw') );

            $email = $this->f3->get('POST.email');
            $naam = $this->f3->get('POST.naam');
            $telefoon = $this->f3->get('POST.telefoon');
            $bericht = $this->f3->get('POST.bericht');

            $receiver->set('From', '"'.$this->f3->get('global.title').'"'.$this->f3->get('from_mail'));
            $receiver->set('To', $this->f3->get('global.infomail'));
            $receiver->set('Subject', 'Contact Formulier '.$this->f3->get('global.title'));
            $receiver->set('Errors-to', $this->f3->get('errors_to'));
            $receiver->set('Reply-To', $email);
            $receiver->set('Content-Type', 'text/html; charset=ISO-8859-1\r\n');

            $sender->set('From', '"'.$this->f3->get('global.title').'"'.$this->f3->get('from_mail'));
            $sender->set('To', $email);
            $sender->set('Subject', 'Contact Formulier '.$this->f3->get('global.title'));
            $sender->set('Errors-to', $this->f3->get('errors_to'));
            $sender->set('Content-Type', 'text/html; charset=ISO-8859-1\r\n');

            $message_receiver = 'Hallo,';
            $message_receiver .= '<br>';
            $message_receiver .= '<br>';
            $message_receiver .= 'Op de '.$this->f3->get('global.title').' site is een contact formulier ingevult.';
            $message_receiver .= '<br>';
            $message_receiver .= '<br>';
            $message_receiver .= '<table>';
            $message_receiver .= "<tr><td>Naam:</td><td>$naam</td></tr>";
            $message_receiver .= "<tr><td>Email:</td><td>$email</td></tr>";
            $message_receiver .= "<tr><td>Telefoon:</td><td>$telefoon</td></tr>";
            $message_receiver .= "<tr><td>Bericht:</td><td>$bericht</td></tr>";
            $message_receiver .= '</table>';
            $message_receiver .= '<br>';
            $message_receiver .= 'Groetjes <br>'.$this->f3->get('global.title').' App :D';

            $message_sender = "Hallo $naam,";
            $message_sender .= '<br>';
            $message_sender .= '<br>';
            $message_sender .= 'We hebben uw bericht ontvangen en zullen spoedig met u contact opnemen.';
            $message_sender .= '<br>';
            $message_sender .= '<br>';
            $message_sender .= 'Groetjes <br>'.$this->f3->get('global.organisation');


            $sent_receiver = $receiver->send($message_receiver, TRUE);
            $sent_sender = $sender->send($message_sender, TRUE);
            $this->f3->reroute('/');
        }
        $this->f3->reroute('/');
    }
}
