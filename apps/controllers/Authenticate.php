<?php

class Authenticate extends Controller{

// Function thats needs to be done before routing the whole page.
    function beforeroute(){
    }

// Function to view Login Page.
    function Login(){
        $this->f3->set('title', 'Login');
        $template = new Template;
        echo $template->render('template_login.htm');
    }

// Function to view Login Page. BYPASS
    function LoginBypass(){
        $this->f3->set('title', 'Login');
        $template = new Template;
        echo $template->render('template_login_bypass.htm');
    }

// Function to Logout and clear the SESSION Data.
    function Logout(){
        $this->f3->clear('SESSION');
        $this->f3->reroute('/');
    }

// Function Autenticate users and set SESSION information.
    function Check(){
        $email = $this->f3->get('POST.email');

        $user = new Users_Model($this->db);
        $user->getByEmail($email);

        if($user->dry()){
            $this->f3->set('SESSION.email', $email);
            $this->f3->reroute('/newuser');
        }

        if( $email == $user->email){
            $this->f3->set('SESSION.email', $user->email);
            $this->f3->set('SESSION.nickname', $user->nickname);
            $this->f3->set('SESSION.uid', $user->id);
            $this->f3->set('SESSION.lvl', $user->lvl);
            $this->f3->set('SESSION.is_admin', $user->is_admin);
            $this->f3->reroute('/app/dashboard');
        } else {
            $this->f3->set('SESSION.email', $email);
            $this->f3->reroute('/newuser');
        }
    }

// Function to view new user page.
    function newUser(){
        $this->f3->set('title', 'Nieuwe Gebruiker');
        $template = new Template;
        $this->f3->set('random', "Speler_".mt_rand(100000, 999999));
        echo $template->render('template_newuser.htm');
    }

// Function add new user.
    function newUserAdd(){
        if($this->f3->exists('POST.create')){

            $email = $this->f3->get('POST.email');
            $lvl = $this->f3->get('POST.lvl');

            $user = new Users_Model($this->db);
            $user->add();

            $this->f3->set('SESSION.email', $user->email);
            $this->f3->set('SESSION.nickname', $user->nickname);
            $this->f3->set('SESSION.uid', $user->id);
            $this->f3->set('SESSION.lvl', $user->lvl);
            $this->f3->set('SESSION.is_admin', $user->is_admin);
            $this->f3->reroute('/app');
        } else {
            $this->f3->reroute('/login');
        }
    }
}
