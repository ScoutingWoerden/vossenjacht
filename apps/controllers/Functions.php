<?php

class Functions extends Controller{

// Loads the render speed.
    function pageLoadedIn(){
        return "...";
    }

// Load the changelog.
    function Changelog(){
        $markdown = new Markdown;
        $file = $this->f3->read('CHANGELOG.md');
        echo "<link rel='stylesheet' href='/public/css/changelog.css'>";
        echo $markdown->convert($file);
    }

// Get Gravatar Image if avaible.
    function Gravatar(){
        $email = $this->f3->get('SESSION.email');
        return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) );
    }

// convert Date's into howlong.
    function NiceTime($time_ago){
        if(empty($time_ago)){
            return "???";
        }
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "net";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "een minuut";
            } else{
                return "$minutes minuten";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "een uur";
            } else{
                return "$hours uren";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "gisteren";
            } else{
                return "$days dagen";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "een week";
            }else{
                return "$weeks weken";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "een maand";
            }else{
                return "$months maanden";
            }
        }
        //Years
        else{
            if($years==1){
                return "een jaar";
            }else{
                return "$years jaren";
            }
        }
    }

}
