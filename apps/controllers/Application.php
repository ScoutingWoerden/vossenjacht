<?php

class Application extends Controller{

// Function to start fisrt page of the application.
    function Dashboard(){
        //Base
        $functions = new Functions;
        $this->f3->set('title', 'Dashboard');
        $this->f3->set('view', 'application/dashboard.htm');
        $this->f3->set('func_gravatar', $functions->Gravatar());

        //Application
        $uid = $this->f3->get('SESSION.uid');
        $user = new Users_Model($this->db);
        $user->getById($uid);
            //Questions Query
            $uid = $this->f3->get('SESSION.uid');
            $questions=$this->db->exec("SELECT q.*,u.id as u_id, u.lvl as u_lvl ,a.answer as u_answer, a.id as u_answerid FROM questions AS q LEFT JOIN user_answers AS a ON q.id=a.qid AND a.uid='$uid' LEFT JOIN users AS u ON a.uid=u.id ORDER BY q.id");
            $this->f3->set('questions', $questions);

            //Progress Query
            $progress = $this->db->exec("SELECT COUNT(CASE WHEN a.answer = q.answer THEN 1 END) AS correct, COUNT(CASE WHEN a.answer != q.answer THEN 1 END) AS incorrect, COUNT(CASE WHEN a.answer = q.answer THEN 1 END) * 100 / (select count(*) from questions) as correct_perc, COUNT(CASE WHEN a.answer != q.answer THEN 1 END) * 100 / (select count(*) from questions) as incorrect_perc, (SELECT COUNT(*) FROM questions) as total FROM user_answers AS a LEFT JOIN questions AS q ON a.qid=q.id WHERE a.uid='$uid';");
            $this->f3->set('progress', $progress);

            //Ranklist Quesry
            $ranklist = $this->db->exec("SELECT RANK() OVER (ORDER BY correct desc, started DESC) AS rank, a.uid, u.nickname, COUNT(CASE WHEN a.answer = q.answer THEN 1 END) AS correct, (select count(*) from users WHERE lvl = '$user->lvl') as total, u.aangemaakt as started FROM user_answers AS a LEFT JOIN questions AS q ON a.qid=q.id LEFT JOIN users as u ON a.uid=u.id  GROUP BY a.uid;");
            $this->f3->set('ranklist', $ranklist);

            //Show user time
            $started = $functions->Nicetime($user->aangemaakt);
            $this->f3->set('started', $started);


        //Render
        $template = new Template;
        echo $template->render('template_app.htm');
    }

// Function to check is answer is OK.
    function updateAnswer(){
        $answers = new Answers_Model($this->db);

        if($this->f3->exists('POST.create')){
            $answers->add();
            $this->f3->reroute('/app/dashboard');
        }
        if($this->f3->exists('POST.update')){
            $answers->edit($this->f3->get('POST.id'));
            $this->f3->reroute('/app/dashboard');
        }
    }

// Function to show questions
    function Questions(){
        $admin = $this->f3->get('SESSION.is_admin');
        if($admin == true){
            //Base
            $functions = new Functions;
            $this->f3->set('title', 'Vragen Overzicht');
            $this->f3->set('view', 'application/questions.htm');
            $this->f3->set('func_gravatar', $functions->Gravatar());

            //Application
            $email = $this->f3->get('SESSION.email');

            $questions = new Questions_Model($this->db);
            $this->f3->set('questions', $questions->all());

            //Render
            $template = new Template;
            echo $template->render('template_app.htm');
        } else{
            $this->f3->reroute('/app/dashboard');
        }
    }

// Function to show questions
    function updateQuestion(){
        $questions = new Questions_Model($this->db);

        if($this->f3->exists('POST.update')){
            $questions->edit($this->f3->get('POST.id'));
            $this->f3->reroute('/app/questions');
        }
    }
}
