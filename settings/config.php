<?php die();

/*
[globals]

; Application Settings
TZ			= Europe/Amsterdam
AUTOLOAD=apps/controllers/|apps/models/|vendor/others/
UI=apps/views/
DEBUG=3
CACHE=true

; Database Connection
dbconn = "mysql:host=HOST;port=3306;dbname=DATABASENAME"
dbuser = "DATABASEUSER"
dbpass = "DATABASEPASSWORD"

; Global Configuration
global[title] = "Vossenjacht 2020"
global[short_title] = "VS20"
global[description] = "Vossenjacht 2020"
global[organisation] = "Scouting Woerden"
global[organisation_site] = "https://scowo.nl"
global[developer] = "Jeffrey Röling"
global[framework] = "F3 Framework"
global[version] = "0.4.4"
global[infomail] = "jeffrey@roling.nu>"

; App Specifiec
app[donate] = "0"
app[donate_link] = ""
app[lvl_easy] = "Makkelijk - 6 jaar of jonger"
app[lvl_normal] = "Normaal - 7 t/m 12 jaar"
app[lvl_hard] = "Moeilijk - 13 jaar of ouder"
app[online] = "0"
app[done] = "0"
app[when_day] = 11
app[when_month] = 4
app[when_year] = 2020
app[when_hour] = 12
app[when_minute] = 00
app[when] = "Op 11 april 2020 om 12.00 uur gaat de Vossenjacht App open (scowo.nl/vossenjacht) tot 16.00 uur. Het spel duurt ongeveer 3 uur."


[mailer]
; smtp config
smtp.host = 
smtp.port = 465
smtp.user = 
smtp.pw = 
; scheme could be SSL or TLS
smtp.scheme =

; optional mail settings
from_mail = 
from_name = Vossenjacht Scouting Woerden
; mail to receive bounced mails
errors_to = 
; used mail for replies to the sent mail
reply_to = 

*/

;?>
