<?php die();

/*
[routes]

; Authenticate
GET /login=Authenticate->Login
GET /logout=Authenticate->Logout
POST /authenticate=Authenticate->Check
GET /newuser=Authenticate->newUser
POST /newuser=Authenticate->newUserAdd

; Functions
GET /changelog=Functions->Changelog

; Webpage
GET /=Application_Pub->Home
GET /home=Application_Pub->Home
GET /how=Application_Pub->How
POST /mail=Application_Pub->Mail
GET /ranklist=Application_Pub->Ranklist

; Application
GET /app=Authenticate->Login
GET /app/bypass=Authenticate->LoginBypass
GET /app/dashboard=Application->Dashboard
POST /app/answer/update=Application->updateAnswer
GET /app/questions=Application->Questions
POST /app/questions/update=Application->updateQuestion
;?>
