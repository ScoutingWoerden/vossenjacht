<?php

// Include the F3 Framework Base
    $f3=require('vendor/f3/base.php');

// Load Configurations
    $f3->config('settings/config.php');
    $f3->config('settings/routes.php');

// Start new Session
    new Session();

// Error Handler
    $f3->set('ONERROR',function($f3){echo Template::instance()->render('template_error.htm');});
    //$f3->redirect('GET /', f3->get('global.organisation_site'));
    
// Execute the Applications
    $f3->run();

