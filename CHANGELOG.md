#CHANGELOG

##0.4.4 (20 October 2020) -> Stable
* CHANGE:   Change mail settings for using params.

##0.4.3 (15 April 2020) -> Stable
* ADDED:    Ranklist per lvl.

##0.4.2 (11 April 2020) -> Stable
* Hot-Fix:  Timezone was wrong, wasn't set to Europa/Amsterdam.

##0.4.1 (6 April 2020) -> Stable
* Fix:      Small fix in ranklist view for user. Now it works per catogorie instead of all users.
* ADDED:    Nickname with default is "speler_randomnumber" but this could be changed.

##0.4 (28 March 2020) -> Stable
* ADDED:    Webpage for user who viewing the site before the app is started.

##0.3 (26 March 2020) -> Development
* ADDED:    Progress information to see how far you are.
* ADDED:    Ranklist details.

##0.2 (25 March 2020) -> Development
* ADDED:    Question overview for users to fill in. Response in color if right or wrong.
* ADDED:    Question edit overview for admins to change question.

##0.1 (24 March 2020) -> Development
* INFO:     First setup of the Apllication Template
* INFO:     First setup of the Application Base.
* ADDED:    Fat Free Framework 3.7.1
* ADDED:    TinyMCE 4.7.12
* ADDED:    jQuery.js 3.4.1
* ADDED:    FontAwesome 5.13.0
* ADDED:    Bootstrap 4.4.1
