# ScoWo Digitale Vossenjacht
Digitale vossenjacht is een idee van een aantal Scouts  van Scouting Woerden normaal gesproken is deze in de stad van worden tijdens paasweekend. Maar helaas ivm corona in 2020 kon deze niet plaatsvinden en is er een digitale variant bedacht.

## Hoe werkt het spel.
Op website van bedrijven in woerden is de vraag gesteld om een digitale vos op hun site te plaatsen, op het moment dat de digitale vossenjacht plaatsvond (zijn nu dus offline). Op die website stond bijvoorbeeld een foto met een plaatje "Krokodil" bijvoorbeeld. In de webpagina van het spel stond bijvoorbeeld de hint "De enige gele supermarkt in woerden" dit is Jumbo bijvoorbeeld daar stond dan een foto van een krokodil die de leden vervolgens invulde op de site als dat goed was werdt die groen als dat fout is rood. Bij rood kon je hem nogmaals doen als je hem gevonden had.

Er zijn 3 mogelijk levels deze kunnen ook anders gemaakt worden maar hiervoor is in eerste instantie gekozen.
1. EASY - oftwel makkelijk (Makkelijk - 6 jaar of jonger)
2. NORMAL - oftwel normaal (Makkelijk - 6 jaar of jonger)
3. HARD - oftwel moeilijk (Makkelijk - 6 jaar of jonger)

Zodra iemand een level gekozen heeft kan die deze **NIET** wijzigen (eventueel wel handmatig in de database).

## Het Grafische verhaal
Afhakelijk van hoeveel vossen er zijn zullen er plaatjes gemaakt worden en rondgemaild naar bedrijven die bereid zijn om deze op hun site/social media te plaatsen om de dag dat het gebeurd.
Eventueele banners en logos kunnen gebruikt worden.

**Banner** = 1940x960 pixels en staat in *./public/img/banner.jpg* (deze zelf nog aanpassen).
**App_Logo** = 400x400 (oftwel vierkant) en staat in *./public/img/app_logo.png* (deze zelf not aanpassen).
**Logo** = 400x400 (oftwel vierkant) en staat in *./public/img/logo.png* (deze zelf not aanpassen bijvoorbeeld je Scouting Groep Logo).
**Favicon** = 256x256 (oftwel vierkant) en staat in *./public/img/favicon.ico* (let op dit moet wel een ico file zijn).

## Het Technische verhaal
Okay het technische verhaal achter de site. 
Gebruikers kunnen zodra de site online is doormiddel van hun email een antwoordenblad aanmaken dit gebeurd direct nadat het email is ingevuld mochten ze de pagina wegklikken en hetzelfde email weer invullen krijgen ze hun antwoordenblad met eventueel ingevulde vragen dus weer voor hun neus.

De applicatie draait in PHP en is gemaakt op het [Fat Free Framework](https://fatfreeframework.com/), en er wordt gebruik gemaakt van een MariaDB Database.

**De installatie stappen zijn als volgt.**
1. Upload alle bestanden in deze repo naar de webserver.
2. Wijzig de config in ./settings/config.php daarbij wijzig je de secties.
    1. *;Database Connection* naar wat het van toepassing is voor de database die je gaat gebruiken.
    2. *;Global Configuration* waarbij eventuele titles veranders kunnen worden zoals naam van Scouting Groep.
    3. *;Mail Configuration* hier maakt alleen het contact formulier gebruik van.
4. Importeerd de *database.sql* file in de database server.
5. Als het goed is moet de site nu draaien en zouden er geen fout meldingen moeten zijn.

**Opzetten van het spel.**
Omdat het niet mogelijk is om via een knop vragen toe te voegen moet dit via de database in het tabel *questions* voor elke vraag zal er dus even een regel moeten worden gemaakt per vraag 1. Vanuit de site is het wel mogelijk de vraag aan te passen als administrator. (administrator wordt je door in de database table users "is_admin" op 1 te zetten. In de webpagina verschijnd dan onder je mailadress een kopje "Vragen wijzigen").

**App config**
In de ./settings/config.php is ook een gedeelte genaamd *; App Specifiec*
Zo kan bijvoorbeeld de doneer link ingegeven worden of zelf uitgezet (standard staat dat uit).
De *app[online]* is om gebruikers toegang te geven, door deze naar een 1 te wijzigen.
De *app[done]* zal naar 1 gezet moeten worden als het spel afgelopen is even als de *app[online]*.
met de *app[when_*]* kan zo worden ingesteld dat de remaining time op de voorpagina van de site zo staat dat gebruikers zien hoelang nog duurt voordat spel gestard word. Handmattig moet nogsteeds de *app[online]* naar 1 gezet worden.

## Bijdragen
Bijdragen zijn altijd welkom om zo de applicatie/spel te verbeteren.


